﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AgriFriendApi.Migrations
{
    public partial class addnewtableset : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "Farmers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NIC",
                table: "Farmers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "Farmers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Street",
                table: "Farmers",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "DivSecretarys",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<int>(nullable: false),
                    Phone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    ContactPerson = table.Column<string>(nullable: true),
                    ContactMobile = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DivSecretarys", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GovijanaSevas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<int>(nullable: false),
                    Phone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    ContactPerson = table.Column<string>(nullable: true),
                    ContactMobile = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GovijanaSevas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GramasevaDivisions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<int>(nullable: false),
                    Phone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    ContactPerson = table.Column<string>(nullable: true),
                    ContactMobile = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GramasevaDivisions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AgricultureResearchDivisions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<int>(nullable: false),
                    Phone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    ContactPerson = table.Column<string>(nullable: true),
                    ContactMobile = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    GovijanaSevaId = table.Column<string>(nullable: true),
                    GovijanaSevaId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgricultureResearchDivisions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AgricultureResearchDivisions_GovijanaSevas_GovijanaSevaId1",
                        column: x => x.GovijanaSevaId1,
                        principalTable: "GovijanaSevas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Lands",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    OtherName = table.Column<string>(nullable: true),
                    Acre = table.Column<int>(nullable: false),
                    Rood = table.Column<int>(nullable: false),
                    Perch = table.Column<int>(nullable: false),
                    NorthEnd = table.Column<string>(nullable: true),
                    SouthEnd = table.Column<string>(nullable: true),
                    WestEnd = table.Column<string>(nullable: true),
                    EastEnd = table.Column<string>(nullable: true),
                    District = table.Column<string>(nullable: true),
                    DivSecretaryId = table.Column<int>(nullable: false),
                    GovijanaSevaId = table.Column<int>(nullable: false),
                    AgricultureResearchDivisionId = table.Column<int>(nullable: false),
                    GramasevaDivisionId = table.Column<int>(nullable: false),
                    Village = table.Column<string>(nullable: true),
                    FieldName = table.Column<string>(nullable: true),
                    FieldNumber = table.Column<string>(nullable: true),
                    SourceOfWater = table.Column<string>(nullable: true),
                    IrrigationAuthority = table.Column<string>(nullable: true),
                    Oriention = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lands", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Lands_DivSecretarys_DivSecretaryId",
                        column: x => x.DivSecretaryId,
                        principalTable: "DivSecretarys",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Lands_GovijanaSevas_GovijanaSevaId",
                        column: x => x.GovijanaSevaId,
                        principalTable: "GovijanaSevas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Lands_GramasevaDivisions_GramasevaDivisionId",
                        column: x => x.GramasevaDivisionId,
                        principalTable: "GramasevaDivisions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AgricultureResearchDivisions_GovijanaSevaId1",
                table: "AgricultureResearchDivisions",
                column: "GovijanaSevaId1");

            migrationBuilder.CreateIndex(
                name: "IX_Lands_DivSecretaryId",
                table: "Lands",
                column: "DivSecretaryId");

            migrationBuilder.CreateIndex(
                name: "IX_Lands_GovijanaSevaId",
                table: "Lands",
                column: "GovijanaSevaId");

            migrationBuilder.CreateIndex(
                name: "IX_Lands_GramasevaDivisionId",
                table: "Lands",
                column: "GramasevaDivisionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AgricultureResearchDivisions");

            migrationBuilder.DropTable(
                name: "Lands");

            migrationBuilder.DropTable(
                name: "DivSecretarys");

            migrationBuilder.DropTable(
                name: "GovijanaSevas");

            migrationBuilder.DropTable(
                name: "GramasevaDivisions");

            migrationBuilder.DropColumn(
                name: "City",
                table: "Farmers");

            migrationBuilder.DropColumn(
                name: "NIC",
                table: "Farmers");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "Farmers");

            migrationBuilder.DropColumn(
                name: "Street",
                table: "Farmers");
        }
    }
}
