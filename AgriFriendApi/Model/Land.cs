﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AgriFriendApi.Model
{
    public class Land
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string OtherName { get; set; }
        public int Acre { get; set; }
        public int Rood { get; set; }
        public int Perch { get; set; }
        public string NorthEnd { get; set; }
        public string SouthEnd { get; set; }
        public string WestEnd { get; set; }
        public string EastEnd { get; set; }

        public string District { get; set; }

        [ForeignKey("DivSecretaryId")]
        public int DivSecretaryId { get; set; }

        [ForeignKey("GovijanaSevaId")]
        public int GovijanaSevaId { get; set; }

        public int AgricultureResearchDivisionId { get; set; }

        [ForeignKey("GramasevaDivisionId")]
        public int GramasevaDivisionId { get; set; }

        public string Village { get; set; }

        public string FieldName { get; set; }

        public string FieldNumber { get; set; }

        public string SourceOfWater { get; set; }

        public string IrrigationAuthority { get; set; }

        public string Oriention { get; set; }

    }
}
