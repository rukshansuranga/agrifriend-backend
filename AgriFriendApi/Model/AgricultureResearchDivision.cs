﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AgriFriendApi.Model
{
    public class AgricultureResearchDivision
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string ContactPerson { get; set; }
        public string ContactMobile { get; set; }
        public string Address { get; set; }
        [ForeignKey("GovijanaSevaId")]
        public string GovijanaSevaId { get; set; }
    }
}
