﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgriFriendApi.Model
{
    public class Farmer
    {
        public int Id { get; set; }

        public int Name { get; set; }

        public string Street { get; set; }
        public string City { get; set; }

        public string NIC { get; set; }

        public string Phone { get; set; }
    }
}
