﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgriFriendApi.Model
{
    public class GovijanaSeva
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string ContactPerson { get; set; }
        public string ContactMobile { get; set; }
        public string Address { get; set; }
        public ICollection<Land> Lands { get; set; }
        public ICollection<AgricultureResearchDivision> AgricultureResearchDivisions { get; set; }
    }
}
