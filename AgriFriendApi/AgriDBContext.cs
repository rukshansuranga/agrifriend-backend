﻿using AgriFriendApi.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgriFriendApi
{
    public class AgriDBContext : DbContext
    {
        public DbSet<Farmer> Farmers { get; set; }
        public DbSet<Land> Lands { get; set; }
        public DbSet<DivSecretary> DivSecretarys { get; set; }
        public DbSet<GovijanaSeva> GovijanaSevas { get; set; }
        public DbSet<AgricultureResearchDivision> AgricultureResearchDivisions { get; set; }
        public DbSet<GramasevaDivision> GramasevaDivisions { get; set; }
        
        public AgriDBContext(){ }
        public AgriDBContext(DbContextOptions<AgriDBContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //if (optionsBuilder.IsConfigured == false)
            //{
            //    var connectionString = "Data Source=.;Initial Catalog=Agri1;Integrated Security=True";

            //    optionsBuilder.UseSqlServer(connectionString);
            //}
        }
    }
}
