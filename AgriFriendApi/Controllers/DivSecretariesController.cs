﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AgriFriendApi;
using AgriFriendApi.Model;

namespace AgriFriendApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DivSecretariesController : ControllerBase
    {
        private readonly AgriDBContext _context;

        public DivSecretariesController(AgriDBContext context)
        {
            _context = context;
        }

        // GET: api/DivSecretaries
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DivSecretary>>> GetDivSecretarys()
        {
            return await _context.DivSecretarys.ToListAsync();
        }

        // GET: api/DivSecretaries/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DivSecretary>> GetDivSecretary(int id)
        {
            var divSecretary = await _context.DivSecretarys.FindAsync(id);

            if (divSecretary == null)
            {
                return NotFound();
            }

            return divSecretary;
        }

        // PUT: api/DivSecretaries/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDivSecretary(int id, DivSecretary divSecretary)
        {
            if (id != divSecretary.Id)
            {
                return BadRequest();
            }

            _context.Entry(divSecretary).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DivSecretaryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DivSecretaries
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<DivSecretary>> PostDivSecretary(DivSecretary divSecretary)
        {
            _context.DivSecretarys.Add(divSecretary);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDivSecretary", new { id = divSecretary.Id }, divSecretary);
        }

        // DELETE: api/DivSecretaries/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<DivSecretary>> DeleteDivSecretary(int id)
        {
            var divSecretary = await _context.DivSecretarys.FindAsync(id);
            if (divSecretary == null)
            {
                return NotFound();
            }

            _context.DivSecretarys.Remove(divSecretary);
            await _context.SaveChangesAsync();

            return divSecretary;
        }

        private bool DivSecretaryExists(int id)
        {
            return _context.DivSecretarys.Any(e => e.Id == id);
        }
    }
}
